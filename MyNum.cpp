#include <stdexcept>
#include "MyNum.h"

MyNum::MyNum(const MyNum& other) {
    this->base = other.base;
    this->digits_str = other.digits_str;
    this->alphabet = other.alphabet;
}

MyNum::MyNum(int base, string digits_str) {
    this->base = base;
    this->digits_str = digits_str;
    for (int i = 0; i < base; i++) {
        alphabet += get_char(i);
    }
}

int MyNum::get_digit(char c) {
    return alphabet.find(c);
}

char MyNum::get_char(int i) {
    if (i < 10) {
        return static_cast<char>('0' + i);
    } else {
        return static_cast<char>('a' + (i - 10));
    }
}

MyNum& MyNum::add(MyNum other) {
    if (base != other.base) throw invalid_argument("numbers have different bases");
    string res_digits;

    int md = 0, n = max(digits_str.size(), other.digits_str.size());
    for (int i = 0; i < n; i++) {
        int sum = get_digit(get_from_end(i)) + other.get_digit(other.get_from_end(i)) + md;
        res_digits.insert(res_digits.begin(), get_char(sum % base));
        md = sum / base;
    }
    if (md != 0) {
        res_digits.insert(res_digits.begin(), get_char(md));
    }

    MyNum* c = new MyNum(base, res_digits);
    return *c;
}

MyNum& MyNum::subtract(MyNum other) {
    if (base != other.base) throw invalid_argument("numbers have different bases");
    string res_digits;

    int md = 0, n = max(digits_str.size(), other.digits_str.size());
    for (int i = 0; i < n; i++) {
        int sum = get_digit(get_from_end(i)) - other.get_digit(other.get_from_end(i)) + md;
        if (sum < 0) {
            sum += base;
            md = -1;
        }
        res_digits.insert(res_digits.begin(), get_char(sum % base));
    }

    MyNum* c = new MyNum(base, res_digits);
    return c->compact();
}

MyNum &MyNum::mult(MyNum other) {
    if (base != other.base) throw invalid_argument("numbers have different bases");

    int n = digits_str.size(), t = other.digits_str.size(), b = 0;
    string res_digits(n + t - 1, '0');
    MyNum* c = new MyNum(base, res_digits);

    for (int i = 0; i < n; i++) {
        b = 0;
        for (int j = 0; j < t; j++) {
            int sum = get_digit(c->get_from_end(i + j)) +
                      get_digit(get_from_end(i)) * get_digit(other.get_from_end(j)) +
                      b;
            string part_res = get_num(sum, base);
            c->digits_str[n + t - 2 - i - j] = (part_res.size() == 1) ? part_res[0] : part_res[1];
            b = (part_res.size() == 1) ? 0 : get_digit(part_res[1]);
        }
    }

    if (b != 0) {
        c->digits_str.insert(res_digits.begin(), get_char(b));
    }

    return *c;
}

pair<MyNum, MyNum> MyNum::div(MyNum other) {
    if (base != other.base) throw invalid_argument("numbers have different bases");

    MyNum r(*this);
    MyNum q = zero(base);

    while (r.gte(other)) {
        r = r.subtract(other);
        q = q.add(one(base));
    }

    pair<MyNum, MyNum> res(q, r);
    return res;
}

char MyNum::get_from_end(int index) {
    int rev_ind = digits_str.size() - 1 - index;
    return rev_ind < 0 ? '0' : digits_str[rev_ind];
}

MyNum& MyNum::compact() {
    if (digits_str.size() != 1) {
        int i;
        for (i = 0; digits_str[i] == '0' && i < digits_str.size(); i++) {}
        digits_str = digits_str.substr(i);
    }
    return *this;
}

string MyNum::get_num(int n, int base) {
    string res(" ");
    int i = 0, x = n, q = x / base;
    res[0] = get_char(x - q * base);
    while (q > 0) {
        i++;
        x = q;
        q = x / base;
        res.insert(res.begin(), get_char(x - q * base));
    }
    return res;
}

int MyNum::get_num() {
    int sum = 0;
    for (int i = digits_str.size() - 1, mult = 1; i >= 0; i--, mult *= base) {
        sum += get_digit(digits_str[i]) * mult;
    }
    return sum;
}

bool MyNum::gte(MyNum other) {
    return this->get_num() >= other.get_num();
}

MyNum MyNum::one(int base) {
    return MyNum(base, "1");
}

MyNum MyNum::zero(int base) {
    return MyNum(base, "0");
}
































