#include <iostream>
#include <chrono>
#include "MyNum.h"

using namespace std;
using namespace std::chrono;

const int iterations = 10000;


long long int add_time(MyNum a, MyNum b) {
    long long int res = 0;
    for (int i = 0; i < iterations; i++) {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        a.add(b);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        res += duration_cast<microseconds>(t2 - t1).count();
    }
    return res / iterations;
}

long long int substract_time(MyNum a, MyNum b) {
    long long int res = 0;
    for (int i = 0; i < iterations; i++) {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        a.subtract(b);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        res += duration_cast<microseconds>(t2 - t1).count();
    }
    return res / iterations;
}

long long int mult_time(MyNum a, MyNum b) {
    long long int res = 0;
    for (int i = 0; i < iterations; i++) {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        a.mult(b);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        res += duration_cast<microseconds>(t2 - t1).count();
    }
    return res / iterations;
}

long long int div_time(MyNum a, MyNum b) {
    long long int res = 0;
    for (int i = 0; i < iterations; i++) {
        high_resolution_clock::time_point t1 = high_resolution_clock::now();
        a.div(b);
        high_resolution_clock::time_point t2 = high_resolution_clock::now();

        res += duration_cast<microseconds>(t2 - t1).count();
    }
    return res / iterations;
}

int main() {
    MyNum a(10, "20000");
    MyNum b(10, "11111");

    pair<MyNum, MyNum> res = a.div(b);
    cout << a.add(b) << endl
         << a.subtract(b) << endl
         << a.mult(b) << endl
         << res.first << res.second << endl;

    cout << add_time(a, b) << endl
         << substract_time(a, b) << endl
         << mult_time(a, b) << endl
         << div_time(a, b) << endl;
}
