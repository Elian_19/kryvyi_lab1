#ifndef ARITHMETIC_MY_NUM_H
#define ARITHMETIC_MY_NUM_H

#include <string>
#include <iostream>

using namespace std;

class MyNum {
private:
    int base;
    string digits_str;
    string alphabet;

    int get_digit(char c);
    static char get_char(int i);
    MyNum& compact();

public:
    MyNum(const MyNum& other);
    MyNum(int base, string digits_str);

    MyNum one(int base);
    MyNum zero(int base);

    MyNum& add(MyNum other);
    MyNum& subtract(MyNum other);
    MyNum& mult(MyNum other);
    bool gte(MyNum other);
    pair<MyNum, MyNum> div(MyNum other);

    static string get_num(int n, int base);
    int get_num();

    char get_from_end(int index);

    friend ostream& operator<<(ostream &stream, const MyNum &num) {
        return stream << "(" << num.digits_str << " / " << num.base << ")";
    }
};


#endif //ARITHMETIC_MY_NUM_H
